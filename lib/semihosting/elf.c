#include <assert.h>
#include <stdint.h>

#include <common/debug.h>

#include <lib/semihosting.h>
#include <lib/semihosting/elf.h>

uintptr_t elf64_load(const char *path)
{
    int fd;
    size_t n;
    int err;

    INFO("Loading %s\n", path);

    fd = semihosting_file_open(path, FOPEN_MODE_RB);
    assert(fd > 0);

    struct Elf64_Header hdr = {0};
    n = sizeof(hdr);
    err = semihosting_file_read(fd, &n, (uintptr_t)&hdr);
    assert(!err);
    assert(n == sizeof(hdr));
    assert(hdr.e_ident[EI_MAG0] == ELFMAG0);
    assert(hdr.e_ident[EI_MAG1] == ELFMAG1);
    assert(hdr.e_ident[EI_MAG2] == ELFMAG2);
    assert(hdr.e_ident[EI_MAG3] == ELFMAG3);
    assert(hdr.e_ident[EI_CLASS] == ELFCLASS64);
    assert(hdr.e_phnum == 1);
    assert(hdr.e_phentsize == sizeof(struct Elf64_Phdr));

    err = semihosting_file_seek(fd, hdr.e_phoff);
    assert(!err);

    struct Elf64_Phdr phdr = {0};
    n = sizeof(phdr);
    err = semihosting_file_read(fd, &n, (uintptr_t)&phdr);
    assert(!err);
    assert(n == sizeof(phdr));

    INFO("e_entry: 0x%llx, p_offset: 0x%llx, p_filesz: 0x%llx, p_paddr: 0x%llx\n",
        hdr.e_entry, phdr.p_offset, phdr.p_filesz, phdr.p_paddr);

    err = semihosting_file_seek(fd, phdr.p_offset);
    assert(!err);

    n = phdr.p_filesz;
    err = semihosting_file_read(fd, &n, (uintptr_t)phdr.p_paddr);
    assert(!err);
    assert(n == phdr.p_filesz);

    return hdr.e_entry;
}
